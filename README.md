# Void Linux Container Images

**This is a free-floating (undocked) fork of <https://github.com/void-linux/void-containers>.**

This repo contains what is needed to build void-linux OCI container
images. There are 3 images provided for each libc (`glibc` or `musl`):

- `void-LIBC`: This image contains far fewer packages and uses a
  `noextract` file to prevent certain directories from being added to
  the image. These images average 40-65MB.

- `void-LIBC-full`: Large image based on the base-minimal package and
  does not contain a `noextract` configuration. If you want something
  that is as close to a full void VM as possible, this is the image you
  want to start with.These images average 80-135MB.

- `void-LIBC-busybox`: This image is the same as the `void-LIBC` image
  above, but uses busybox instead of GNU coreutils. Note that this is
  not a well tested configuration with Void, but if you want a very
  small image, busybox is a good way to get it. These images average 15-40MB.

These images are available for the following OCI platforms:

- `linux/amd64`
- `linux/386` (`glibc` only)
- `linux/arm64`
- `linux/arm/v7`
- `linux/arm/v6`

```
REPOSITORY                                                   TAG       SIZE
registry.gitlab.com/mjstrauss/void-linux/void-musl-busybox   20230901  18.2 MB
registry.gitlab.com/mjstrauss/void-linux/void-glibc-busybox  20230901  43.1 MB
registry.gitlab.com/mjstrauss/void-linux/void-musl-full      20230901  92.3 MB
registry.gitlab.com/mjstrauss/void-linux/void-glibc-full     20230901  145 MB
registry.gitlab.com/mjstrauss/void-linux/void-musl           20230901  47.4 MB
registry.gitlab.com/mjstrauss/void-linux/void-glibc          20230901  69.1 MB
```

## Building locally

With `podman`:

1. Install and set up `podman`.
2. Build the image:
```sh
podman build --target "image-<default|full|busybox>" --build-arg="LIBC=<glibc|musl>" . --tag <yourtag>
```

With `just`:

1. Install [just](https://github.com/casey/just).
2. Build all images:
```sh
just build-all
```