registry := "registry.gitlab.com"
registry_user := "mjstrauss"
registry_project := "void-linux"

default: build-all push-all

@build-all:
	for target in default full busybox; do \
		for libc in glibc musl; do \
			just build $target $libc; \
		done; \
	done

@push-all:
	for target in default full busybox; do \
		for libc in glibc musl; do \
			just push $target $libc; \
		done; \
	done

tag target libc:
	#!/usr/bin/env bash
	set -euo pipefail
	TAG="void-{{libc}}"
	if [ "{{target}}" != "default" ]
	then
		TAG="$TAG-{{target}}"
	fi
	cat > .tag <<<"$TAG:`date +%Y%m%d`"

@build target="default" libc="musl": (tag target libc)
	# target = default | full | busybox
	# libc = glibc | musl
	podman build --target=image-{{ target }} --build-arg=LIBC={{ libc }} --tag `cat .tag` .

@push target libc: (tag target libc)
	echo Pushing {{ registry }}/{{ registry_user }}/{{ registry_project }}/`cat .tag`
	podman tag `cat .tag` {{ registry }}/{{ registry_user }}/{{ registry_project }}/`cat .tag`
	podman push {{ registry }}/{{ registry_user }}/{{ registry_project }}/`cat .tag`

